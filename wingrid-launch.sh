#!/bin/bash
# program for jumping through the grids, as built  by antiX team
# docs: https://downloads.tuxfamily.org/antix/docs-antiX-15/FAQ/wingrid.html
# repo: https://github.com/antiX-Linux/wingrid-antix

file_prefix=/tmp/.wg-state-
exist=0
grid[0]=wingrid-maximize.sh
grid[1]=wingrid-left.sh
grid[2]=wingrid-right.sh
grid[3]=wingrid-top.sh
grid[4]=wingrid-bottom.sh
grid[5]=wingrid-topleft.sh
grid[6]=wingrid-topright.sh
grid[7]=wingrid-bottomleft.sh
grid[8]=wingrid-bottomright.sh
grid[9]=wingrid-center.sh

for i in {0..9}; do
    if test -f "${file_prefix}${i}"; then
	exist=1
	actual_grid=${i}
    fi
done

if [ "$exist" = "0" ]; then
    touch ${file_prefix}0
fi 

${grid[$actual_grid]}
new_grid=`expr $actual_grid + 1`
if [ $new_grid -gt 9 ];
then
    new_grid=0
fi
mv ${file_prefix}${actual_grid} ${file_prefix}${new_grid}
